import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      name: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      all: [],
      currentPage: 'all'
    }
  }


  handleNameChange = (event) => {
    this.setState({
      name: event.target.value
    })
  }

  handleAddress1Change = (event) => {
    this.setState({
      address1: event.target.value
    })
  }

  handleAddress2Change = (event) => {
    this.setState({
      address2: event.target.value
    })
  }

  handleCityChange = (event) => {
    this.setState({
      city: event.target.value
    })
  }

  handleStateChange = (event) => {
    this.setState({
      state: event.target.value
    })
  }




  onSubmit = () => {
    let payload = {
      name: this.state.name,
      address1: this.state.address1,
      address2: this.state.address2,
      city: this.state.city,
      state: this.state.state
    }
    console.log(this.state.all)
    this.setState((state) => ({ all: [...state.all, payload] }))
    this.setState({ name: '' })
    this.setState({ address1: '' })
    this.setState({ address2: '' })
    this.setState({ city: '' })
    this.setState({ state: '' })
  }


  render() {
    return (
      <div className="App">

      <div className="sidenav">
      <ul>
        <li onClick={() => this.setState({ currentPage: 'all' })}><a href="#">List of Institutions</a></li>
        <li onClick={() => this.setState({ currentPage: 'add' })}><a href="#">Add Institution</a></li>        
      </ul>
      </div>

      <div className="main">
      
          {this.state.currentPage === 'all' ? (

            
          <ul className="display-list">
          <li className='list-header'>
              <span>Name</span>
              <span>Address 1</span>
              <span>Address 2</span>
              <span>City</span>
              <span>State</span>          
          </li>
            {this.state.all.map((item, index) => (<li key={index}>
              <span>{item.name}</span>
              <span>{item.address1}</span>
              <span>{item.address2}</span>
              <span>{item.city}</span>
              <span>{item.state}</span>
            </li>))}
          </ul>

          ) : (

<div className="wrapper">
            <h2 className="text-center">Institution</h2>
      <div className="row">
        <div className="field-name">Name: </div>
        <div className="field-holder">
              <input type="text" value={this.state.name} onChange={this.handleNameChange} className="form form-control" placeholder="Name" />
        </div>
      </div>


      <div className="row">
        <div className="field-name">Address: </div>
        <div className="field-holder">
        <input type="text" value={this.state.address1} onChange={this.handleAddress1Change} className="form form-control" placeholder="line 1" />
        <input type="text" value={this.state.address2} onChange={this.handleAddress2Change} className="form form-control" placeholder="line 2" />
        <input type="text" value={this.state.city} onChange={this.handleCityChange} className="form form-control" placeholder="city" />
        <input type="text" value={this.state.state} onChange={this.handleStateChange} className="form form-control" placeholder="state" />
        </div>
      </div>
      <div className="row btn-container">
      <button className="btn btn-primary" onClick={this.onSubmit}> SUBMIT </button>
      </div>
</div>

          )}

      </div>

      </div>
    );
  }
}

export default App;
